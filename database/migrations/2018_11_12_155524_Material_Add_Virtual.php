<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MaterialAddVirtual extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::table('materiales', function (Blueprint $table) {
           $table->boolean('virtual')->default(0);
       });

       $materiales = [
         ['id' => 20, 'nombre' => 'TPV', 'peso' => 10000, 'genera_numero' => true, 'virtual' => false],
         ['id' => 21, 'nombre' => 'Toner', 'peso' => 500, 'genera_numero' => false, 'virtual' => false],
         ['id' => 22, 'nombre' => 'Armario Rack', 'peso' => 10000, 'genera_numero' => true, 'virtual' => false],
         ['id' => 23, 'nombre' => 'Proyector', 'peso' => 2000, 'genera_numero' => true, 'virtual' => false],
         ['id' => 24, 'nombre' => 'Hora Taller Interno', 'peso' => 0, 'genera_numero' => false, 'virtual' => true],
         ['id' => 25, 'nombre' => 'Hora Taller Externa', 'peso' => 0, 'genera_numero' => false, 'virtual' => true],
         ['id' => 26, 'nombre' => 'Donación', 'peso' => 0, 'genera_numero' => false, 'virtual' => true],
         ['id' => 27, 'nombre' => 'Formación', 'peso' => 0, 'genera_numero' => false, 'virtual' => true],
         ['id' => 28, 'nombre' => 'Charlas', 'peso' => 0, 'genera_numero' => false, 'virtual' => true],
         ['id' => 29, 'nombre' => 'Talleres', 'peso' => 0, 'genera_numero' => false, 'virtual' => true],
         ['id' => 30, 'nombre' => 'Manipulación', 'peso' => 0, 'genera_numero' => false, 'virtual' => true],
         ['id' => 31, 'nombre' => 'Transporte', 'peso' => 0, 'genera_numero' => false, 'virtual' => true],
         ['id' => 32, 'nombre' => 'Borrado Seguro', 'peso' => 0, 'genera_numero' => false, 'virtual' => true],
         ['id' => 33, 'nombre' => 'Disco duro', 'peso' => 400, 'genera_numero' => false, 'virtual' => false],
         ['id' => 34, 'nombre' => 'Descuento', 'peso' => 0, 'genera_numero' => false, 'virtual' => true],
       ];

       DB::table('materiales')->insert($materiales);
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::table('materiales', function (Blueprint $table) {
           $table->dropColumn('virtual');
       });
     }
}
