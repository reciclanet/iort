<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use App\Organizacion;
use App\Persona;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(Persona::class, 10)->create();
        factory(Organizacion::class, 20)->create();

        Model::reguard();
    }
}
