<?php

namespace App;

class Lote extends Model
{
    /**
     * The relationships to always eager-load.
     *
     * @var array
     */
    protected $with = ['responsable'];
       
    public function tipoLote()
    {
        return $this->belongsTo(TipoLote::class);
    }

    public function responsable()
    {
        return $this->morphTo();
    }
    
    public function persona()
    {
        return $this->belongsTo(Persona::class, 'responsable_id')
        ->where('lotes.responsable_type', Persona::class);
    }
    
    public function organizacion()
    {
        return $this->belongsTo(Organizacion::class, 'responsable_id')
        ->where('lotes.responsable_type', Organizacion::class);
    }
    
    
    public function materiales()
    {
        return $this->hasMany(LoteMaterial::class);
    }

    public function getDates()
    {
        return ['created_at', 'updated_at', 'fecha'];
    }

    public function getMaterialesAgrupados($campos = [], $filtros = [])
    {
      $materiales = [];
      foreach($this->materiales as $material){
          
          if (!$material->filtrar($filtros)) continue;
          
        $codigoMaterial = "";
        foreach($campos as $campo){
          $codigoMaterial .= $material[$campo] . "_";
        }

        if(array_key_exists($codigoMaterial, $materiales)){
          $materiales[$codigoMaterial]->cantidadSuma++;
          $materiales[$codigoMaterial]->descuentoSuma += $material->descuentoFinal ?? 0;
          $materiales[$codigoMaterial]->precioSuma += $material->precio ?? 0;

        } else {
          $material->cantidadSuma = 1;
          $material->descuentoSuma = $material->descuentoFinal ?? 0;
          $material->precioSuma = $material->precio ?? 0;
          $materiales[$codigoMaterial] = $material;
        }
      }
      return $materiales;
    }
    
    public function addLoteMaterial($datos, $cantidad, $tipo_descuento)
    {
      for($contador = 0; $contador < $cantidad; $contador++){
        $loteMaterial = new LoteMaterial($datos);
        if ($this->tipoLote->genera_numero && !$loteMaterial->txae && $loteMaterial->material->genera_numero) {
          $loteMaterial->codigo = LoteMaterial::getCodigoSiguiente();
        }
        if ($tipo_descuento == '%') {
            $loteMaterial->descuento = ($loteMaterial->descuento / 100) * $loteMaterial->precio;
        }

        $this->materiales()->save($loteMaterial);
      }
    }
    
    public function delete()
    {
      $this->materiales->each(function (LoteMaterial $loteMaterial) {
        $loteMaterial->delete();
      });

      return parent::delete();
    }
}
