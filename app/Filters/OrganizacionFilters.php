<?php

namespace App\Filters;

class OrganizacionFilters extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = ['q'];
    
    protected function q($patron)
    {
        $q = "%";
        if ($patron) {
            $q = '%' . $patron . '%';
        }
        
        $this->builder
            ->where('razon_social', 'like', $q)
            ->orWhere('poblacion', 'like', $q)
            ->orWhere('telefono_1', 'like', $q)
            ->orWhere('telefono_2', 'like', $q);           
    }
}
