<?php

namespace App\Filters;

class LoteFilters extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = ['q'];
    
    protected function q($patron)
    {
        $q = "%";
        if ($patron) {
            $q = '%' . $patron . '%';
        }

        $this->builder
            ->where('id', 'like', $q)
                ->orWhere(function ($query) use ($q) {
                  if (env('DB_CONNECTION') == 'sqlite') {
                    $query->whereRaw('strftime("%d/%m/%Y", fecha) like ?', $q);
                  } else {
                    $query->whereRaw('DATE_FORMAT(fecha, "%d/%m/%Y") like ?', $q);
                  }
              })
             ->orWhereHas('tipoLote', function ($query) use ($q) {
                  $query->where('nombre', 'like', $q);
              })
            ->orWhere('descripcion', 'like', $q)
            ->orWhereHas('organizacion', function ($query) use ($q) {
                     $query->where('razon_social', 'like', $q);
                 })
             ->orWhereHas('persona', function ($query) use ($q) {
                 $query->whereRaw('(nombre || CASE WHEN apellido_1 = "" THEN "" ELSE " " || apellido_1 END || CASE WHEN apellido_2 = "" THEN "" ELSE " " || apellido_2 END)  like ?', $q);
             });
    }
}
