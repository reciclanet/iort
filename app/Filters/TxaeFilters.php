<?php

namespace App\Filters;

class TxaeFilters extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = ['q'];
    
    protected function q($patron)
    {
        $q = "%";
        if ($patron) {
            $q = '%' . $patron . '%';
        }
        
        $this->builder
            ->where('codigo', 'like', $q)
             ->orWhereHas('material', function ($query) use ($q) {
                  $query->where('nombre', 'like', $q);
              })
              ->orWhereHas('lote', function ($query) use ($q) {
                  if (env('DB_CONNECTION') == 'sqlite') {
                    $query->whereRaw('strftime("%Y%m%d", fecha) like ?', $q);
                  } else {
                    $query->whereRaw('DATE_FORMAT(fecha, "%Y%m%d") like ?', $q);
                  }
              })
            ->orWhereHas('lote.organizacion', function ($query) use ($q) {
                     $query->where('razon_social', 'like', $q);
                 })
             ->orWhereHas('lote.persona', function ($query) use ($q) {
                 $query->whereRaw('"Particular" like ?', $q);
             });
    }
}
