@extends('layouts.app')

@section('content')

<h1>Lote - {{ $lote->tipoLote->nombre }}</h1>

@include('lotes/form')

@include ('lote_material.index')

<div>
    <a href="{{ URL::previous() }}" class="btn btn-danger">Volver</a>
</div>
@endsection

