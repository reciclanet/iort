@extends('layouts.informe')

@section('datos-personales')
<table class="tablaDatosPersonales">
    <tr>
        <td colspan="4">
            <h2>Número de Referencia: IORT {{ $lote->id }}<h2>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <h3>Colaborador</h3>
        </td>
        <td colspan="2" style="text-align: right;">Fecha: {{ $lote->fecha->format('d/m/Y') }}</td>
    </tr>
    <tr class="borde">
        <td class="negrita">CONTACTO</td>
        <td colspan="3">{{ $lote->responsable->getNombreDescriptivo() }}</td>
    </tr>
    <tr class="borde">
        <td class="negrita">TELÉFONO</td>
        <td>{{ $lote->responsable->telefono_1 }}</td>
        <td class="negrita">EMAIL</td>
        <td>{{ $lote->responsable->email }}</td>
    </tr>
</table>
@endsection

@section('content')
    @include ('lote_material.informe')
@endsection
