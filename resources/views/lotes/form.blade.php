
{!! Form::model($lote, ['id' => 'frmLote', 'action' => ['LoteController@update', $lote->id], 'method' => 'PATCH', 'class' => 'ajax']) !!}

@include ('layouts.errors')

<div class='row'>
    {{ Form::label('fecha', 'Fecha', ['class' =>"offset-md-6 col-md-2"]) }}
    <div class="form-group col-md-4 {{ $errors->has('fecha') ? 'error' : '' }}">
        {{ Form::date('fecha', $lote->fecha, ['class' =>"form-control"] )}}
    </div>
    {{ Form::label('descripcion', 'Descripción', ['class' =>"col-md-2"]) }}
    <div class="form-group col-md-10  {{ $errors->has('descripcion') ? 'error' : '' }}">
        {{ Form::text('descripcion', null, ['class' =>"form-control"] )}}
    </div>
</div>

<div class="alert alert-danger print-error-msg" style="display:none">
    <ul></ul>
</div>

<div class='float-right'>
    <button id="btn_actualizar_lote" type="submit" class="btn btn-primary">Actualizar</button>
</div>

{!! Form::close() !!}

@push('scripts')
<script>

    function printErrorMsg(idFormulario, msg) {
        $(".print-error-msg", idFormulario).find("ul").html('');
        $(".print-error-msg", idFormulario).css('display', '');
        $.each(msg.errors, function (key, value) {
            $(".print-error-msg", idFormulario).find("ul").append('<li>' + value + '</li>');
        });
    }

    $('form.ajax').on('submit', function (evento) {
        evento.preventDefault();

        let formulario = this;
        let datosFormulario = new FormData(formulario);

        axios.post(formulario.action, datosFormulario)
                .then(function (response) {
                    flash('Actualizado');
                })
                .catch(function (error) {
                    if (error.response.data.message) {
                        flash(error.response.data.message, 'danger');
                    }
                    printErrorMsg("#" + formulario.id, error.response.data);
                });
    });

</script>
@endpush

