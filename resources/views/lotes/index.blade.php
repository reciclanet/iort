@extends('layouts.app')

@section('content')
<h1>Lotes</h1>

<form class="float-right" role="search" method="GET">
    <div class="input-group mb-3">
        <input type="text" name="q" class="form-control col-sm-6" placeholder="Buscar..." value="{{ request('q') }}">
        <div class="input-group-append">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
                <i class="fa fa-search fa-lg"></i>
                Buscar
            </button>
        </div>
    </div>
</form>

<div>
    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Fecha</th>
                <th>Tipo</th>
                <th>Descripción</th>
                <th>Responsable</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($lotes as $lote)
            <tr>
                <td><a href="{{ url('lotes/' . $lote->id ) }}">{{ $lote->id }}</a></td>
                <td>{{ $lote->fecha->format('d/m/Y') }}</td>
                <td>{{ $lote->tipoLote->nombre }}</td>
                <td>{{ $lote->descripcion }}</td>
                <td>{{ $lote->responsable->getNombreDescriptivo() }}</td>
                    <td>
                        <button type="button" class="btn btn-danger eliminar" value="{{$lote->id}}">Eliminar</button>
                    </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $lotes->appends($_GET)->links() }}
</div>
@endsection
@push('scripts')
<script>

    function cargarBotones() {
        $(".eliminar").each(function (index, item) {
            $(this).unbind("click");
            $(this).on("click", function (event) {
                if (!confirm('Estás seguro de que quieres eliminar el lote ' + $(this).attr('value') + '?')) {
                    event.preventDefault();
                    return;
                }
                url = 'lotes/' + $(this).attr('value');
                $.ajax({
                    type: "DELETE",
                    url: url,
                    dataType: "json",
                    success: function () {
                        location.reload();
                    }
                });
            });
        });
    }

    jQuery(document).ready(function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        cargarBotones();
    });


</script>
@endpush