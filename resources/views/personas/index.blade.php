@extends('layouts.app')

@section('content')
<h1>Personas</h1>


<div>
    <a href="{{ url('personas/create')}}" class="btn btn-primary navbar-left">Nueva</a>

    <form class="float-right" role="search" method="GET">
        <div class="input-group mb-3">
            <input type="text" name="q" class="form-control col-sm-6" placeholder="Buscar..." value="{{ request('q') }}">
            <div class="input-group-append">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
                    <i class="fa fa-search fa-lg"></i>
                    Buscar
                </button>
            </div>
        </div>
    </form>

    <table class="table table-bordered" id="personas-table">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Apellido 1</th>
                <th>Apellido 2</th>
                <th>Población</th>
                <th>Teléfono 1</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($personas as $persona)
            <tr>
                <td><a href="/personas/{{$persona->id}}">{{$persona->nombre}}</a></td>
                <td>{{ $persona->apellido_1 }}</td>
                <td>{{ $persona->apellido_2 }}</td>
                <td>{{ $persona->poblacion }}</td>
                <td>{{ $persona->telefono_1 }}</td>
                <td>{{ $persona->email }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $personas->appends($_GET)->links() }}
</div>
@endsection
