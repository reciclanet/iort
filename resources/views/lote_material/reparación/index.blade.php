@php
    $loteMaterialesFiltrados = $loteMateriales->groupBy('entrada_salida');
    $valorSalvarInformacion = '';
    if($loteMaterialesFiltrados->has('')
            && count($loteMaterialesFiltrados['']) == 1 
            && $loteMaterialesFiltrados[''][0]->descripcion == 'Salvar Informacion') {
        $valorSalvarInformacion = $loteMaterialesFiltrados[''][0]->id;
    }
@endphp

<div class="form-group custom-control custom-checkbox">
  <input type="checkbox" class="custom-control-input" id="chk_salvar_informacion" value="{{ $valorSalvarInformacion }}" {{ $valorSalvarInformacion == '' ? '' : 'checked' }} >
  <label class="custom-control-label" for="chk_salvar_informacion">Se requieres salvar la información</label>
</div>


<h5>Entregado</h5>
<div>
    <table id="tblEntregado" class="table">
        <thead>
            @if (isset($edicion) && $edicion)
            <tr>
                <th>Material</th>
                <th>Marca</th>
                <th>Modelo</th>
                <th>n.º de serie</th>
                <th>Descripción</th>
                <th></th>
            </tr>
            <tr>
                <td>{{ Form::select('material_id', $materiales, null, ['id'=> 'material_id', 'class'=>"form-control select2", 'placeholder' => ''])}}</td>
                <td><input id="marca" class="form-control" type="text"></td>
                <td><input id="modelo" class="form-control" type="text"></td>
                <td><input id="tag" class="form-control" type="text"></td>
                <td><input id="descripcion" class="form-control" type="text"></td>
                <td>
                    <input id="entrada_salida" name="entrada_salida" value="0" type="hidden">
                    <button type="button" class="btn btn-success add-loteMaterial" value="tblEntregado">Añadir</button>
                </td>
            </tr>
            <tr class="alert alert-danger print-error-msg" style="display:none">
                <td colspan="7">
                    <ul></ul>
                </td>
            </tr>
            @endif
            <tr>
                <th>Material</th>
                <th>Marca</th>
                <th>Modelo</th>
                <th>n.º de serie</th>
                <th>Descripción</th>
                @if (isset($edicion) && $edicion)
                <th></th>
                @endif
            </tr>
        </thead>
        <tbody>
            @if ($loteMaterialesFiltrados->has(0))
            @foreach ($loteMaterialesFiltrados[0] as $loteMaterial)
            <tr id="loteMaterial{{$loteMaterial->material_id}}">
                <td>{{ $loteMaterial->material->nombre }}</td>
                <td>{{ $loteMaterial->marca }}</td>
                <td>{{ $loteMaterial->modelo }}</td>
                <td>{{ $loteMaterial->tag }}</td>
                <td>{{ $loteMaterial->descripcion }}</td>
                @if (isset($edicion) && $edicion)
                <td>
                    <button type="button" class="btn btn-danger delete-loteMaterial" value="{{$loteMaterial->id}}">Eliminar</button>
                </td>
                @endif
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>

<h5>Trabajo Realizado</h5>
<div>
    <table id="tblRealizado" class="table">
        <thead>
            @if (isset($edicion) && $edicion)
            <tr>
                <th>Material</th>
                <th>Descripción</th>
                <th>Precio</th>
                <th></th>
            </tr>
            <tr>
                <td>{{ Form::select('material_id', $materiales, null, ['id'=> 'material_id', 'class'=>"form-control select2", 'placeholder' => ''])}}</td>
                <td><input id="descripcion" class="form-control" type="text"></td>
                <td><input id="precio" class="form-control" type="text"></td>
                <td>
                    <input id="entrada_salida" name="entrada_salida" value="1" type="hidden">
                    <button type="button" class="btn btn-success add-loteMaterial" value="tblRealizado">Añadir</button>
                </td>
            </tr>
            <tr class="alert alert-danger print-error-msg" style="display:none">
                <td colspan="7">
                    <ul></ul>
                </td>
            </tr>
            @endif
            <tr>
                <th>Material</th>
                <th>Descripción</th>
                <th>Precio</th>
                @if (isset($edicion) && $edicion)
                <th></th>
                @endif
            </tr>
        </thead>
        <tbody>
            @if ($loteMaterialesFiltrados->has(1))
            @foreach ($loteMaterialesFiltrados[1] as $loteMaterial)
            <tr id="loteMaterial{{$loteMaterial->material_id}}">
                <td>{{ $loteMaterial->material->nombre }}</td>
                <td>{{ $loteMaterial->descripcion }}</td>
                <td>{{ $loteMaterial->precio }}</td>
                @if (isset($edicion) && $edicion)
                <td>
                    <button type="button" class="btn btn-danger delete-loteMaterial" value="{{$loteMaterial->id}}">Eliminar</button>
                </td>
                @endif
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>

@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
function printErrorMsg(idFormulario, msg) {
    $(".print-error-msg", idFormulario).find("ul").html('');
    $(".print-error-msg", idFormulario).css('display', '');
    $.each(msg.errors, function (key, value) {
        $(".print-error-msg", idFormulario).find("ul").append('<li>' + value + '</li>');
    });
}

function cargarJavascriptElementos() {
    cargarBotonesAniadir();
    cargarBotonesEliminar();

    $('.select2').select2({
        placeholder: 'Selecciona el material..',
    });
}

function cargarBotonesEliminar() {
    $(".delete-loteMaterial").each(function (index, item) {
        $(this).unbind("click");
        $(this).on("click", function (event) {
            $.ajax({
                type: "DELETE",
                url: "{{ url('lotes/' . $lote->id .'/loteMateriales/')}}/" + $(this).attr('value'),
                dataType: "json",
                success: function (data) {
                    if (data && data.success) {
                        $('#loteMaterialIndex').html(data.html);
                        cargarJavascriptElementos();
                    } else {
                        alert(data.message);
                    }
                }
            });
        });
    });
}

function cargarBotonesAniadir() {
    $(".add-loteMaterial").each(function (index, item) {
        $(this).unbind("click");
        $(this).on("click", function (event) {
            let idFormulario = "#" + $(this).val();
            
            let datosFormulario = new FormData();

            var controles = $(':input:not(:button)', idFormulario);
            $.each(controles, function (num, objeto) {
                datosFormulario.append(objeto.id, $(objeto).val());
            });
            
            $(".print-error-msg", idFormulario).css('display', 'none');
            
            axios.post('loteMateriales', datosFormulario)
                .then(function (response) {                    
                    if (response.data && response.data.success) {
                        $('#loteMaterialIndex').html(response.data.html);
                        controles.not(':button, :submit, :reset, :hidden')
                                .val('').change()
                                .removeAttr('checked')
                                .removeAttr('selected');
                        cargarJavascriptElementos();
                    } else {
                        alert(response.data.message);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    printErrorMsg(idFormulario, error.response.data);
                });            
        });
    });
}

$('#chk_salvar_informacion').click(function(){
    if($(this).is(':checked')){
        let data = new FormData();
        data.append('descripcion', 'Salvar Informacion');
        data.append('material_id', 24);
    
        axios.post('loteMateriales', data)
            .then(function (response) {
                $('#loteMaterialIndex').html(response.data.html);

                cargarJavascriptElementos();
            })
            .catch(function (error) {
                location.reload();
            });
    } else {
        axios.delete('loteMateriales/' + $(this).val()).then(function (response) {
            $('#loteMaterialIndex').html(response.data.html);

            cargarJavascriptElementos();
        })
        .catch(function (error) {
            location.reload();
        });
    }
});

jQuery(document).ready(function ($) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    cargarJavascriptElementos();
});

</script>
@endpush
