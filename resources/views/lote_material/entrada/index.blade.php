<div>
    <table class="table">
        <thead>
            @if (isset($edicion) && $edicion)
            <tr>
                <th>Material</th>
                <th>Cantidad</th>
                <th>¿Borrado Seguro?</th>
                <th>TXAE</th>
                <th></th>
            </tr>
            <tr>
                <td>{{ Form::select('material_id', $materiales, null, ['id'=> 'material_id', 'class'=>"form-control", 'placeholder' => ''])}}</td>
                <td>{{ Form::number('material_cantidad', null, ['id'=> 'material_cantidad', 'class' =>"form-control"] )}}</td>
                <td>{{ Form::checkbox('material_borrado_seguro', 1, null,['id'=> 'material_borrado_seguro'])}}</td>
                <td>{{ Form::checkbox('material_txae', 1, null,['id'=> 'material_txae'])}}</td>
                <td>
                    {{ Form::hidden('material_entrada_salida', 0,['id'=> 'material_entrada_salida']) }}
                    <button type="button" class="btn btn-success add-loteMaterial">Añadir</button>
                </td>
            </tr>
            <tr class="alert alert-danger print-error-msg" style="display:none">
                <td colspan="7">
                    <ul></ul>
                </td>
            </tr>
            @endif
            <tr>
                <th>Material</th>
                <th>Código</th>
                <th>¿Borrado Seguro?</th>
                <th>TXAE</th>
                @if (isset($edicion) && $edicion)
                <th></th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach ($loteMateriales as $loteMaterial)
            <tr id="loteMaterial{{$loteMaterial->material_id}}">
                <td>{{ $loteMaterial->material->nombre }}</td>
                <td>{{ $loteMaterial->codigo }}</td>
                <td>{{ ($loteMaterial->borrado_seguro)? 'Sí' : 'No' }}</td>
                <td>{{ ($loteMaterial->txae)? 'Sí' : 'No' }}</td>
                @if (isset($edicion) && $edicion)
                <td>
                    <button type="button" class="btn btn-danger delete-loteMaterial" value="{{$loteMaterial->id}}">Eliminar</button>
                    @if (!$loteMaterial->txae)
                    <button type="button" class="btn btn-danger txae-loteMaterial" codigo="{{$loteMaterial->codigo}}" value="{{$loteMaterial->id}}">Marcar Txae</button>
                    @endif
                </td>
                @endif
            </tr>
            @endforeach            
        </tbody>        
    </table>
</div>

@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
function printErrorMsg(msg) {
    $(".print-error-msg").find("ul").html('');
    $(".print-error-msg").css('display', '');
    $.each(msg.errors, function (key, value) {
        $(".print-error-msg").find("ul").append('<li>' + value + '</li>');
    });
}

function cargarJavascriptElementos() {
    cargarBotonesAniadir();
    cargarBotonesEliminar();
    cargarBotonesMarcarTxae();

    $('#material_id').select2({
        placeholder: 'Selecciona el material..',
    });
}

function cargarBotonesEliminar() {
    $(".delete-loteMaterial").each(function (index, item) {
        $(this).unbind("click");
        $(this).on("click", function (event) {
            $.ajax({
                type: "DELETE",
                url: "{{ url('lotes/' . $lote->id .'/loteMateriales/')}}/" + $(this).attr('value'),
                dataType: "json",
                success: function (data) {
                    if (data && data.success) {
                        $('#loteMaterialIndex').html(data.html);
                        cargarJavascriptElementos();
                    } else {
                        alert(data.message);
                    }
                }
            });
        });
    });
}

function cargarBotonesMarcarTxae() {
    $(".txae-loteMaterial").each(function (index, item) {
        $(this).unbind("click");
        $(this).on("click", function (event) {
            if (!confirm('Estás seguro de que quieres marcar el código ' + $(this).attr('codigo') + ' como Txae?')) {
                event.preventDefault();
                return;
            }

            $.ajax({
                type: "PATCH",
                url: "{{ action('TxaeController@update') }}",
                dataType: "json",
                data: {
                    id: $(this).attr('value'),
                    codigo: $(this).attr('codigo')
                },
                success: function () {
                    location.reload();
                }
            });
        });
    });
}

function cargarBotonesAniadir() {
    $(".add-loteMaterial").each(function (index, item) {
        $(this).unbind("click");
        $(this).on("click", function (event) {
            $(".print-error-msg").css('display', 'none');
            var formData = {
                lote_id: <?php echo $lote->id; ?>,
                material_id: $('#material_id').val(),
                cantidad: $('#material_cantidad').val(),
                borrado_seguro: $('#material_borrado_seguro').is(":checked") ? 1 : 0,
                txae: $('#material_txae').is(":checked") ? 1 : 0,
                entrada_salida: $('#material_entrada_salida').val()
            }
            $.ajax({
                type: "POST",
                url: "{{ url('lotes/' . $lote->id .'/loteMateriales')}}",
                dataType: "json",
                data: formData,
                success: function (data) {
                    if (data && data.success) {
                        $('#loteMaterialIndex').html(data.html);
                        $(':input', '#loteMaterialIndex')
                                .not(':button, :submit, :reset, :hidden')
                                .val('').change()
                                .removeAttr('checked')
                                .removeAttr('selected');
                        cargarJavascriptElementos();
                    } else {
                        alert(data.message);
                    }
                },
                error: function (data, textStatus, errorThrown) {
                    printErrorMsg(data.responseJSON);
                }
            });
        });
    });
}

jQuery(document).ready(function ($) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    cargarJavascriptElementos();
});


</script>
@endpush
