@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-sm-6">
    <h1>Datos Organización</h1>
    <dl class="row">
      <dt class="col-sm-4">Fecha de alta:</dt>
      <dd class="col-sm-8">{{ $organizacion->created_at->toDateString() }}</dd>
      <dt class="col-sm-4">Razón Social:</dt>
      <dd class="col-sm-8">{{ $organizacion->razon_social }}</dd>
      <dt class="col-sm-4">Actividad Principal:</dt>
      <dd class="col-sm-8">{{ $organizacion->actividad_principal }}</dd>
      <dt class="col-sm-4">NIF:</dt>
      <dd class="col-sm-8">{{ $organizacion->cif_nif }}</dd>
      <dt class="col-sm-4">Forma jurídica:</dt>
      <dd class="col-sm-8">
        @if ($organizacion->forma_juridica)
          {{ $organizacion->forma_juridica->nombre }}
        @endif
      </dd>
      <dt class="col-sm-4">Dirección:</dt>
      <dd class="col-sm-8">{{ $organizacion->direccion }}</dd>
      <dt class="col-sm-4">CP:</dt>
      <dd class="col-sm-8">{{ $organizacion->cp }}</dd>
      <dt class="col-sm-4">Población:</dt>
      <dd class="col-sm-8">{{ $organizacion->poblacion }}</dd>
      <dt class="col-sm-4">Provincia:</dt>
      <dd class="col-sm-8">
        @if ($organizacion->provincia)
          {{ $organizacion->provincia->nombre }}
        @endif
      </dd>
      <dt class="col-sm-4">Teléfono 1:</dt>
      <dd class="col-sm-8">{{ $organizacion->telefono_1 }}</dd>
      <dt class="col-sm-4">Teléfono 2:</dt>
      <dd class="col-sm-8">{{ $organizacion->telefono_2 }}</dd>
      <dt class="col-sm-4">Email:</dt>
      <dd class="col-sm-8">{{ $organizacion->email }}</dd>
      <dt class="col-sm-4">Página Web:</dt>
      <dd class="col-sm-8">{{ $organizacion->pagina_web }}</dd>
      <dt class="col-sm-6">¿Cómo nos has conocido?:</dt>
      <dd class="col-sm-6">
        @if ($organizacion->tipoConocido)
          {{ $organizacion->tipoConocido->nombre }}
        @endif
      </dd>
      <dt class="col-sm-4">Codigo:</dt>
      <dd class="col-sm-8">{{ $organizacion->codigo }}</dd>
      <dt class="col-sm-4">¿Autoriza logo?:</dt>
      <dd class="col-sm-8">
        @if ($organizacion->autoriza_logo)
          {{ "Sí" }}
        @else
          {{ "No" }}
        @endif
      </dd>
      <dt class="col-sm-4">Notas:</dt>
      <dd class="col-sm-8">{{ $organizacion->notas }}</dd>
      <dt class="col-sm-4">Etiquetas:</dt>
      <dd class="col-sm-8">
        @foreach ($organizacion->tags as $tag)
          <span class="label label-primary">{{ $tag->nombre }}</span>
        @endforeach
      </dd>
      <dt class="col-sm-4"></dt>
      <dd class="col-sm-8">
        @if ($organizacion->logo)
          <img src={{ '/images/logos/' . $organizacion->logo }} width="200"/>
        @endif
      </dd>
    </dl>
  </div>
  <div class="col-sm-6">
    <h2>Lotes</h2>
    <ul>
      @foreach ($organizacion->lotes as $lote)
        <li><a href="{{ url("lotes/$lote->id" )}}">{{ $lote->created_at->format('d/m/Y') . ' - ' . $lote->descripcion }}</a></li>
      @endforeach
    </ul>
  </div>
</div>
  <div class="row" style="clear:both;">
    <div class="col-sm-6">
      <a href="{{ url("organizaciones/$organizacion->id/edit") }}" class="btn btn-primary">Editar</a>
    </div>
    <div class="col-sm-6">
      <a href="{{ action('LoteController@create').'?'.http_build_query(["tipo" => get_class($organizacion), "id" => $organizacion->id]) }}" class="btn btn-primary">Nuevo</a>
    </div>
  </div>
@endsection
