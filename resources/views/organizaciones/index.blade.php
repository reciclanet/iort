@extends('layouts.app')

@section('content')
<h1>Organizaciones</h1>
<a href="{{ url('organizaciones/create')}}" class="btn btn-primary navbar-left">Nueva</a>

<form class="float-right" role="search" method="GET">
    <div class="input-group mb-3">
        <input type="text" name="q" class="form-control col-sm-6" placeholder="Buscar..." value="{{ request('q') }}">
        <div class="input-group-append">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
                <i class="fa fa-search fa-lg"></i>
                Buscar
            </button>
        </div>
    </div>
</form>

<div>
    <table class="table table-bordered" id="organizaciones-table">
        <thead>
            <tr>
                <th>Razón Social</th>
                <th>Población</th>
                <th>Teléfono 1</th>
                <th>Teléfono 2</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($organizaciones as $organizacion)
            <tr>
                <td><a href="/organizaciones/{{$organizacion->id}}">{{$organizacion->razon_social}}</a></td>
                <td>{{ $organizacion->poblacion }}</td>
                <td>{{ $organizacion->telefono_1 }}</td>
                <td>{{ $organizacion->telefono_2 }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $organizaciones->appends($_GET)->links() }}
</div>
@endsection